using System.Collections.Generic;

public class GridFilters
{
    public string GroupOp { get; set; }
    public List<GridRule> Rules { get; set; }
}