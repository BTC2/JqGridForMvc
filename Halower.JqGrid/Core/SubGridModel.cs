using Newtonsoft.Json;

namespace Halower.JqGrid.Core
{
    public class SubGridModel
    {
        [JsonProperty("name")]
        public string[] FiledNames { get; set; }

        [JsonProperty("width")]
        public int[] FiledWidths { get; set; }

        [JsonProperty("align")]
        public string[] FiledAligns { get; set; }

        [JsonProperty("params")]
        public string[] Params { get; set; }
    }
}