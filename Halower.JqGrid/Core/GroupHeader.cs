﻿using Newtonsoft.Json;

namespace Halower.JqGrid.Core
{
    public class GroupHeader
    {
        public GroupHeader(string startColumnName, int numberOfColumns, string titleTextHtml)
        {
            StartColumnName = startColumnName;
            NumberOfColumns = numberOfColumns;
            TitleText = titleTextHtml;
        }
        [JsonProperty("startColumnName")]
        public string StartColumnName { get; set; }

        [JsonProperty("numberOfColumns")]
        public int NumberOfColumns { get; set; }

        [JsonProperty("titleText")]
        public string TitleText { get; set; }
    }
}