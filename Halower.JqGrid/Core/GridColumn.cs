﻿/****************************************************
** 作者： Halower (QQ:121625933)
** 创始时间：2015-02-01
** 描述：jqGrid扩展单元列属性
*****************************************************/

using System;
using Halower.JqGrid.Enum;
using Newtonsoft.Json;

namespace Halower.JqGrid.Core
{
    public class GridColumn
    {
        #region 字段
        private string _aligin;
        private string _index;
        private bool? _search;
        private string _fomatter;
        #endregion

        #region 属性
        [JsonProperty("name")]
        public string Field { get; set; }

        [JsonProperty("index")]
        public string Index
        {
            get { return _index ?? Field; }
            set { _index = value; }
        }

        [JsonProperty("width")]
        public int? Width { get; set; }

        [JsonProperty("align")]
        public string Align
        {
            get { return _aligin ?? "right"; }
            set { _aligin = value; }
        }
        [JsonIgnore]
        public string DisplayName { get; set; }

        [JsonProperty("sortable")]
        public bool Sortable { get; set; }

        [JsonProperty("hidden")]
        public bool Hidden { get; set; }

        [JsonProperty("editable")]
        public bool Editable { get; set; }

        [JsonProperty("search")]
        public bool Search
        {
            get { return _search ?? false; }
            set { _search = value; }
        }

        [JsonProperty("stype")]
        public string SearchType { get; set; }

        [JsonProperty("sorttype")]
        public string SortType { get; set; }

        [JsonProperty("edittype")]
        public string EditType { get; set; }

        [JsonProperty("searchoptions")]
        public string Searchoptions
        {
            get
            {
                return this.SearchFiledType==CellTypes.String? "&{sopt:['eq','bw','cn']}&" : "&{sopt:['eq','lt','gt']}&";
            }
        }

        [JsonProperty("formatter")]
        public string Formatter
        {
            get
            {
                if (!string.IsNullOrEmpty(_fomatter))
                    return "&" + _fomatter + "&";
                return null;
            }
            set { _fomatter = value; }
        }

        [JsonIgnore]
        public CellTypes SearchFiledType { get; set; }

        #endregion

        #region 主要方法

        /// <summary>
        /// 设置列的初始宽度，可用pixels和百分比
        /// </summary>
        /// <param name="width">初始宽度</param>
        /// <returns></returns>
         [Obsolete("已过时,不建议使用")]
        public GridColumn SetWidth(int? width)
        {
            Width = width;
            return this;
        }

        /// <summary>
        /// 定义初始化时，列是否隐藏
        /// </summary>
        [Obsolete("已过时,不建议使用")]
        public GridColumn SetHidden(bool hidden = true)
        {
            Hidden = hidden;
            return this;
        }

        /// <summary>
        /// 定义定义字段是否可编辑
        /// </summary>
        [Obsolete("已过时,不建议使用")]
        public GridColumn SetEditable(ColumnEdits edittype)
        {
            Editable = true;
            EditType = edittype.ToString().ToLower();
            return this;
        }

        /// <summary>
        /// 定义定义字段是否可编辑
        /// </summary>
        [Obsolete("已过时,不建议使用")]
        public GridColumn SetFormatter(string cellformater)
        {
            Formatter = cellformater;
            return this;
        }

        /// <summary>
        /// 定义搜索 
        /// </summary>
        [Obsolete("已过时,不建议使用")]
        public GridColumn SetSearchable(ColumnSearchs columnSearchType = ColumnSearchs.Text)
        {
            Search = true;
            SearchType = columnSearchType.ToString().ToLower();
            return this;
        }

        /// <summary>
        /// 启用排序
        /// </summary>
        /// <param name="columnSortType">排序类型</param>
        /// <returns></returns>
         [Obsolete("已过时,不建议使用")]
        public GridColumn SetSortable(ColumnSorts columnSortType = ColumnSorts.Text)
        {
            Sortable = true;
            SortType = columnSortType.ToString().ToLower();
            return this;
        }

        /// <summary>
        /// 设置字段映射名
        /// </summary>
        /// <param name="field">字段</param>
        /// <param name="name">映射名</param>
        /// <returns></returns>
        [Obsolete("已过时,不建议使用")]
        public GridColumn SetFieldName(string field, string name)
        {
            Field = field;
            DisplayName = name;
            return this;
        }

        /// <summary>
        /// 设置字段映射名
        /// </summary>
        /// <param name="field">字段</param>
        /// <param name="name">映射名</param>
        /// <returns></returns>
        public GridColumn(string field, string name)
        {
            Field = field;
            DisplayName = name;
        }
        #endregion

    }
}