﻿/****************************************************
** 作者： Halower (QQ:121625933)
** 创始时间：2015-02-01
** 描述：jqGrid扩展构造列
*****************************************************/

using System;
using Halower.JqGrid.Core;

namespace Halower.JqGrid.Factory
{
    [Obsolete("已过时不建议使用")]
    public class ColumnFactory
    {
        [Obsolete("已过时不建议使用")]
        public static GridColumn Create(string filedName, string displayName)
        {
            return new GridColumn(filedName, displayName);
        }
    }
}