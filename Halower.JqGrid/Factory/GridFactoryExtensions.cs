using System.Web.Mvc;
using Halower.JqGrid.Factory;

namespace Halower.JqGirdToolKit
{
    /// <summary>
    /// Grid 工厂扩展方法。
    /// </summary>
    public static class GridFactoryExtensions
    {
        /// <summary>
        /// Bootstrap。
        /// </summary>
        /// <param name="htmlHelper">Html 助手。</param>
        /// <returns>Grid 工厂。</returns>
        public static GridFacotory GridToolKit(this HtmlHelper htmlHelper)
        {
            return new GridFacotory(htmlHelper);
        }

        /// <summary>
        /// Bootstrap。
        /// </summary>
        /// <typeparam name="TModel">模型类型。</typeparam>
        /// <param name="htmlHelper">Html 助手。</param>
        /// <returns>Grid 工厂。</returns>
        public static GridFacotory<TModel> GridToolKit<TModel>(this HtmlHelper<TModel> htmlHelper)
        {
            return new GridFacotory<TModel>(htmlHelper);
        }
    }
}