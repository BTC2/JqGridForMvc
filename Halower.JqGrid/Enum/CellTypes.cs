namespace Halower.JqGrid.Enum
{
    public enum CellTypes
    {
        String,
        Number,
        DateTime
    }
}