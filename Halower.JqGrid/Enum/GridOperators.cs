/****************************************************
** 作者： Halower (QQ:121625933)
** 创始时间：2015-02-01
** 描述：jqGrid扩展枚举
*****************************************************/

using System;

namespace Halower.JqGrid.Enum
{
    [Flags]
    public enum GridOperators
    {
        Add=1,
        Edit=2,
        Delete=4,
        Search=8
    }
}