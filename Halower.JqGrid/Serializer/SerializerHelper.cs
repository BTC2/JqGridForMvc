﻿using Newtonsoft.Json;

namespace Halower.JqGrid.Serializer
{
    public static class SerializerHelper
    {
        public static string ToSerializerIgnoreNullValue(this object result)
        {
            return JsonConvert.SerializeObject(result, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        public static string ToSerializer(this object result)
        {
            return JsonConvert.SerializeObject(result);
        }
    }

    
}