﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Halower.JqGrid.Pager;

namespace MVC5Demo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ContentResult GridData(string name)
        {
            var list = new List<Person>();
            for (var i = 0; i < 300000; i++)
            {
                list.Add(new Person { Id = i + 1, Name = "测试" + (i + 1), Skin = "Yellow" + i });
            }
            //支持EF分页排序
            //var context=new PersonContext();
            //context.Persons.Pagination(this);
            return Content(list.Pagination(this));
        }

        public string SubGridData(string name)
        {
            var list = new List<Chinese>();
            for (var i = 0; i < 5; i++)
            {
                list.Add(new Chinese { Id = i + 1, Language = "中文" + name, Name = "子表测试" + (i + name), Country = "中国" + name });
            }
            return list.PushSubGrid(this);
        }
    }

    public class Chinese : Person
    {
        public string Language { get; set; }
        public string Country { get; set; }
    }


    public class ViewDto
    {
        [Display(Name = "编号")]
        public int Id { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "肤色")]
        public string Skin { get; set; }


        [Display(Name = "语言")]
        public string Language { get; set; }

        [Display(Name = "国籍")]
        public string Country { get; set; }
        [Display(Name = "操作")]
        public object Opeator { get; set; }
    }
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Skin { get; set; }
    }
}
