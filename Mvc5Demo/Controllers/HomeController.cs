﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Halower.JqGrid.Pager;

namespace Mvc5Demo.Controllers
{
    public class HomeController : Controller
    {
        public  static readonly List<Person> List = new List<Person>();

        public HomeController()
        {
            for (var i = 0; i < 300000; i++)
            {
                List.Add(new Person { Id = i + 1, Name = "测试" + (i + 1), Skin = "Yellow" + i });
            }
        }
            
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ContentResult GridData(string name)
        {
            
            return Content(List.Pagination(this));
        }

        public ContentResult SubGridasGrid(int id)
        {
            return Content(List.Pagination(this));
        }

        public string SubGridData(string name)
        {
            var list = new List<Chinese>();
            for (var i = 0; i < 5; i++)
            {
                list.Add(new Chinese { Id = i + 1, Language = "中文" + name, Name = "子表测试" + (i + name), Country = "中国" + name });
            }
            return list.PushSubGrid(this);
        }

        public ViewResult SubEditForm(int id)
        {
            var person=List.Find(t => t.Id == id);
            return View(person);
        }
    }

 

    public class Chinese : Person
    {
        public string Language { get; set; }
        public string Country { get; set; }
    }


    public class ViewDto
    {
        [Display(Name = "编号")]
        public int Id { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "肤色")]
        public string Skin { get; set; }


        [Display(Name = "语言")]
        public string Language { get; set; }

        [Display(Name = "国籍")]
        public string Country { get; set; }
        [Display(Name = "操作")]
        public object Opeator { get; set; }
    }
    public class Person
    {
        public int Id { get; set; }
          [Display(Name = "姓名")]
        public string Name { get; set; }
          [Display(Name = "皮肤")]
        public string Skin { get; set; }
    }
}